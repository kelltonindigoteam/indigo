package com.indigomobileapp.util;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.indigomobileapp.interfaces.Constant;
import com.indigomobileapp.model.response.ErrorModel;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import kellton.framwork.interfaces.IScreen;

/***
 * @author ankur.goyal
 *         API Error handle of get@ application
 */

public class VolleyErrorListener implements Response.ErrorListener {
    private final int action;
    private final IScreen screen;

    public VolleyErrorListener(final IScreen screen, final int action) {
        this.screen = screen;
        this.action = action;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String str = "";
        int code = 0;
        ErrorModel errorModel = null;

        if (error != null && error.networkResponse != null && error.networkResponse.data != null) {

            try {
                str = new String(error.networkResponse.data, "UTF-8");
                code = error.networkResponse.statusCode;
                Log.e("Code", "" + code);
                Log.e("str", "" + str);
                errorModel = jsonToArray(str, code);
                if (errorModel != null) {
                    screen.updateUi(false, action, errorModel);
                } else {
                    if (code >= 402 && code < 500) {
                        screen.updateUi(false, action, "Bad Request");
                    } else if (code >= 500)
                        screen.updateUi(false, action, "Server error");
                }
                return;
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        } else if (screen != null)
            if (error instanceof NoConnectionError) {
                Log.e("VollyError", "here1");
                try {
                    Log.e("Response", "" + error);
                    str = new String(error.networkResponse.data, "UTF-8");
                    Log.e("VollyEroor", str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                screen.updateUi(false, action, Constant.NoConnectionError);
            } else if (error instanceof AuthFailureError) {
                screen.updateUi(false, action, Constant.AuthFailureError);
            } else if (error instanceof NetworkError) {
                screen.updateUi(false, action, Constant.NetworkError);
            } else if (error instanceof ParseError) {
                screen.updateUi(false, action, Constant.ParseError);
            } else if (error instanceof ServerError) {
                screen.updateUi(false, action, Constant.ServerError);
            } else if (error instanceof TimeoutError) {
                screen.updateUi(false, action, Constant.TimeoutError);
            }
    }

    /***
     * @param jsonInput
     * @param statusCode
     * @return
     */
    private ErrorModel jsonToArray(String jsonInput, int statusCode) {
        try {
            JSONObject jsonObject = new JSONObject(jsonInput);
            ErrorModel errorModel = new ErrorModel();

            errorModel.setSuccess(jsonObject.optString("success"));
            JSONObject res = jsonObject.optJSONObject("message");
            if (res != null) {
                JSONArray jsonArray = res.optJSONArray("username");
                JSONArray jsonArray1 = res.optJSONArray("email");
                if (jsonArray != null) {
                    errorModel.setMessage(jsonArray.getString(0));
                } else if (jsonArray1 != null) {
                    errorModel.setMessage(jsonArray1.getString(0));
                }
            } else {
                errorModel.setMessage(jsonObject.optString("message"));
            }
            errorModel.setError(jsonObject.optString("error"));
            errorModel.setError_description(jsonObject.optString("error_description"));
            errorModel.setStatusCode(statusCode);
            return errorModel;
        } catch (Exception e) {
            return null;
        }
    }
}
