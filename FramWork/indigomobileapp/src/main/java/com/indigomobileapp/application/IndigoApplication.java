package com.indigomobileapp.application;

import kellton.framwork.application.BaseApplication;
import kellton.framwork.volley.RequestManager;

/**
 * Created by ankur.goyal on 20-10-2016.
 */

public class IndigoApplication extends BaseApplication {
    @Override
    protected void initialize() {
        RequestManager.initializeWith(this, null);
    }
}
