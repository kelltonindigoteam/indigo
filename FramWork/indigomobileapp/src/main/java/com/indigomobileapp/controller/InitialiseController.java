package com.indigomobileapp.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.google.gson.JsonObject;
import com.indigomobileapp.util.VolleyErrorListener;

import java.util.ArrayList;

import kellton.framwork.controller.BaseController;
import kellton.framwork.interfaces.IScreen;
import kellton.framwork.volley.GsonObjectRequest;
import kellton.framwork.volley.RequestManager;

/**
 * Created by ankur.goyal on 20-10-2016.
 * Initialise controller
 */

public class InitialiseController extends BaseController {
    public static final String TAG_TASK_FRAGMENT = InitialiseController.class.getSimpleName();

    /***
     * Get instance of InitialiseController
     * @param action event action
     * @param url request url
     * @param request request body
     * @param headerKey request header key list
     * @param headerValue request header value list
     * @return
     */
    public static InitialiseController getInstance(int action, String url, String request, ArrayList<String> headerKey, ArrayList<String> headerValue) {
        InitialiseController controller = new InitialiseController();
        Bundle bundle = new Bundle();
        bundle.putInt(EVENT_ACTION, action);
        bundle.putString(EVENT_URL, url);
        bundle.putString(REQUEST, request);
        bundle.putStringArrayList(HEADER_VALUE, headerValue);
        bundle.putStringArrayList(HEADER_KEY, headerKey);
        controller.setArguments(bundle);
        return controller;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callAPI(getIScreen());
    }

    private void callAPI(final IScreen iScreen) {
        RequestManager.addRequest(new GsonObjectRequest<JsonObject>(url, header, request, JsonObject.class, new VolleyErrorListener(iScreen, eventAction)) {
            @Override
            protected void deliverResponse(JsonObject response) {
                //Update ui
                iScreen.updateUi(true, eventAction, response);
                if (getActivity() == null)
                    return;
                //if controller fragment exist than remove it
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment controllerFragment = fm.findFragmentByTag(TAG_TASK_FRAGMENT);
                if (controllerFragment != null) {
                    fm.beginTransaction().remove(controllerFragment).commit();
                }
            }
        });
    }

}
