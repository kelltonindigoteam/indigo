package com.indigomobileapp.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.indigomobileapp.model.response.session.SessionModel;
import com.indigomobileapp.util.VolleyErrorListener;

import java.util.ArrayList;
import java.util.HashMap;

import kellton.framwork.controller.BaseController;
import kellton.framwork.interfaces.IScreen;
import kellton.framwork.volley.MetaRequest;
import kellton.framwork.volley.RequestManager;

/**
 * Created by ankur.goyal on 01-11-2016.
 * Session request controller
 */

public class SessionController extends BaseController {
    public static final String TAG_TASK_FRAGMENT = SessionController.class.getSimpleName();

    /***
     * return SessionController instance
     * @param action event action
     * @param url request url
     * @param request request body
     * @param headerKey request header key
     * @param headerValue request header value
     * @return
     */
    public static SessionController getInstance(int action, String url, String request, ArrayList<String> headerKey, ArrayList<String> headerValue) {
        SessionController controller = new SessionController();
        Bundle bundle = new Bundle();
        bundle.putInt(EVENT_ACTION, action);
        bundle.putString(EVENT_URL, url);
        bundle.putString(REQUEST, request);
        bundle.putStringArrayList(HEADER_VALUE, headerValue);
        bundle.putStringArrayList(HEADER_KEY, headerKey);
        controller.setArguments(bundle);
        return controller;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callAPI(getIScreen());
    }

    /***
     * Request for session
     * @param iScreen
     */
    private void callAPI(final IScreen iScreen) {
        //Create a Hash Map for mete data (Header parsing)
        // key is header key
        // value is our custom value
        HashMap<String, String> metaMap = new HashMap<>();
        metaMap.put("X-Session-Token", "session_token");
        RequestManager.addRequest(new MetaRequest<SessionModel>(url, header, request, SessionModel.class,metaMap, new VolleyErrorListener(iScreen, eventAction)) {
            @Override
            protected void deliverResponse(SessionModel response) {
                iScreen.updateUi(true, eventAction, response);
                if (getActivity() == null)
                    return;
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment sessionFragment = fm.findFragmentByTag(TAG_TASK_FRAGMENT);
                if (sessionFragment != null) {
                    fm.beginTransaction().remove(sessionFragment).commit();
                }
            }
        });
    }
}
