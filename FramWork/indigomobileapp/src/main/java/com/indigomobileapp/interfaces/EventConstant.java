package com.indigomobileapp.interfaces;

/**
 * Created by ankur.goyal on 21-10-2016.
 */

public interface EventConstant {
    int INITIALISE_EVENT = 1;
    int SESSION_EVENT = 2;
}
