package com.indigomobileapp.interfaces;

/**
 * Created by ankur.goyal.
 * This class handle Constant
 */
public interface Constant {

    /**
     * Volley Error constant
     */
    String NoConnectionError = "No connectivity: We are having trouble with the internet. Please check your connection.";
    String AuthFailureError = "Authentication Failure";
    String NetworkError = "The connection is weak. Please try again later.";
    String ParseError = "Parse Error";
    String ServerError = "Server Error";
    String TimeoutError = "The connection is weak. Please try again later.";

}

