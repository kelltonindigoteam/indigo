package com.indigomobileapp.interfaces;

import com.indigomobileapp.BuildConfig;

/**
 * Created by ankur.goyal on 20-10-2016.
 */

public interface APIConstant {
    String SUBSCRIPTION_KEY="5468657365206172656E2774207468652064726F69647320796F75277265206C6F6F6B696E6720666F722E";
    String BASE_URL = BuildConfig.BASE_URL;
    String INITIALISE_URL = BASE_URL + "ext/Resources/simple?rs=city&rs=Station&rs=Market&rs=CountryName&rs=TimeZone&rs=Title&rs=Suffix&rs=PassengerLimit&rs=PaxType&allowedOnly=True&rs=TravelCommerceSetting&rs=AvailabilityFilterSetting&rs=UnitGroupColorMapping&rs=fees&rs=PromoBinValidation&rs=DomainSettings&rs=TermsAndConditions&rs=Currency&rs=CcAvenuePaymentOption&cultureCode=en-US&rs=CcAvenueParamSetDetail&activeOnly=True";
    String SESSION_URL = BASE_URL+"api/Session/Create?version=2&_dc="+System.currentTimeMillis();
}
