package com.indigomobileapp.views.activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.JsonObject;
import com.indigomobileapp.R;
import com.indigomobileapp.controller.InitialiseController;
import com.indigomobileapp.controller.SessionController;
import com.indigomobileapp.interfaces.APIConstant;
import com.indigomobileapp.interfaces.EventConstant;
import com.indigomobileapp.model.request.SessionRequest;
import com.indigomobileapp.model.response.ErrorModel;
import com.indigomobileapp.model.response.session.SessionModel;

import kellton.framwork.util.ConnectivityUtils;
import kellton.framwork.util.ToastUtils;

/**
 * Created by ankur.goyal on 20-10-2016.
 */

public class SplashActivity extends IndigoActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData(EventConstant.SESSION_EVENT);
                getData(EventConstant.INITIALISE_EVENT);
            }
        });

    }

    @Override
    public void updateUi(boolean status, int actionID, Object serviceResponse) {
        removeProgressDialog();
        if (!status) {
            if (serviceResponse instanceof ErrorModel) {
                ErrorModel errorModel = (ErrorModel) serviceResponse;
                ToastUtils.showToast(this, errorModel.getError());
            } else {
                ToastUtils.showToast(this, serviceResponse.toString());
            }
            return;
        }
        switch (actionID) {
            case EventConstant.INITIALISE_EVENT:
                if (serviceResponse instanceof JsonObject) {
                    JsonObject object = (JsonObject) serviceResponse;
                    Log.e("response", object.toString());
                }
                break;
            case EventConstant.SESSION_EVENT:
                if (serviceResponse instanceof SessionModel) {
                    SessionModel object = (SessionModel) serviceResponse;
                    Log.e("session", object.getSession_token());
                }
                break;
        }

    }

    @Override
    public void getData(int actionID) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        switch (actionID) {
            case EventConstant.INITIALISE_EVENT:
                InitialiseController controllerFragment = (InitialiseController) fm.findFragmentByTag(InitialiseController.TAG_TASK_FRAGMENT);
                // If the Fragment is non-null, then it is currently being
                // retained across a configuration change.
                if (controllerFragment == null) {
                    controllerFragment = InitialiseController.getInstance(EventConstant.INITIALISE_EVENT, APIConstant.INITIALISE_URL, null, null, null);
                    fm.beginTransaction().add(controllerFragment, InitialiseController.TAG_TASK_FRAGMENT).commit();
                }
                break;
            case EventConstant.SESSION_EVENT:
                SessionController sessionController = (SessionController) fm.findFragmentByTag(SessionController.TAG_TASK_FRAGMENT);
                // If the Fragment is non-null, then it is currently being
                // retained across a configuration change.
                if (sessionController == null) {
                    SessionRequest request = new SessionRequest();
                    request.setSubscriptionKey(APIConstant.SUBSCRIPTION_KEY);
                    sessionController = SessionController.getInstance(EventConstant.SESSION_EVENT, APIConstant.SESSION_URL, request.getPayLoadString(), null, null);
                    fm.beginTransaction().add(sessionController, SessionController.TAG_TASK_FRAGMENT).commit();
                }
                break;
        }
    }

    @Override
    public void showProgress() {
        showProgressDialog("", false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeProgressDialog();
    }
}
