package com.indigomobileapp.views.activites;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.indigomobileapp.R;

import kellton.framwork.ui.BaseActivity;

/**
 * Created by ankur.goyal on 20-10-2016.
 */

public class MainActivity extends IndigoActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void updateUi(boolean status, int actionID, Object serviceResponse) {

    }

    @Override
    public void getData(int actionID) {

    }

    @Override
    public void showProgress() {

    }
}
