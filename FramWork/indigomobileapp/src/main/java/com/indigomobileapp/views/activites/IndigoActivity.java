package com.indigomobileapp.views.activites;

import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.KeyEvent;

import com.indigomobileapp.views.dialog.ProgressHUD;

import kellton.framwork.ui.BaseActivity;
import kellton.framwork.volley.RequestManager;

/**
 * Created by ankur.goyal on 21-10-2016.
 */

public abstract class IndigoActivity extends BaseActivity {
    private ProgressHUD mProgressDialog;

    /**
     * Shows a simple native progress dialog<br/>
     * Subclass can override below two methods for custom dialogs- <br/>
     * 1. showProgressDialog <br/>
     * 2. removeProgressDialog
     *
     * @param bodyText
     */
    public void showProgressDialog(String bodyText, final boolean isRequestCancelable) {
        try {
            if (isFinishing()) {
                return;
            }
            if (mProgressDialog == null) {
                mProgressDialog = ProgressHUD.show(this, "", isRequestCancelable, false, null);
                mProgressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_CAMERA || keyCode == KeyEvent.KEYCODE_SEARCH) {
                            return true; //
                        } else if (keyCode == KeyEvent.KEYCODE_BACK && isRequestCancelable) {
                            Log.e("Ondailogback", "cancel dailog");
                            RequestManager.cancelRequest();
                            dialog.dismiss();
                            return true;
                        }
                        return false;
                    }
                });
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (Exception e) {

        }
    }


    /**
     * Removes the simple native progress dialog shown via showProgressDialog <br/>
     * Subclass can override below two methods for custom dialogs- <br/>
     * 1. showProgressDialog <br/>
     * 2. removeProgressDialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {

        }
    }
}
