package com.indigomobileapp.views.dialog;



import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indigomobileapp.R;


/***
 *
 * @author Ankurgoyal
 * Custom progress dilaog
 *
 */
public class ProgressHUD extends Dialog {
	private Context activity;
	public ProgressHUD(Context context) {
		super(context);
		this.activity = context;
	}

	public ProgressHUD(Context context, int theme) {
		super(context, theme);
		this.activity = context;
	}


	public void onWindowFocusChanged(boolean hasFocus){
		ImageView imageView = (ImageView) findViewById(R.id.loadingAnimationGraphic);
		imageView.startAnimation(
				AnimationUtils.loadAnimation(activity, R.anim.rotation) );
    }

	public void setMessage(CharSequence message) {
		if(message != null && message.length() > 0) {
			findViewById(R.id.loadingText).setVisibility(View.VISIBLE);
			TextView txt = (TextView)findViewById(R.id.loadingText);
			txt.setText(message);
			txt.invalidate();
		}
	}
	/***
	 * show progress dialog
	 * @param context
	 * @param message
	 * @param indeterminate
	 * @param cancelable
	 * @param cancelListener
	 * @return
	 */
	public static ProgressHUD show(Context context, CharSequence message, boolean indeterminate, boolean cancelable,
								   OnCancelListener cancelListener) {
		ProgressHUD dialog = new ProgressHUD(context, R.style.ProgressHUD);
		dialog.setTitle("");
		dialog.setContentView(R.layout.spinner_layout);
		if(message == null || message.length() == 0) {
			dialog.findViewById(R.id.loadingText).setVisibility(View.GONE);
		} else {
			TextView txt = (TextView)dialog.findViewById(R.id.loadingText);
			txt.setText(message);
		}
		dialog.setCancelable(cancelable);
		dialog.setOnCancelListener(cancelListener);
		dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		dialog.getWindow().getAttributes().gravity= Gravity.CENTER;
		dialog.show();
		return dialog;
	}
}
