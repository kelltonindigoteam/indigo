package com.indigomobileapp.model.response;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 21-10-2016.
 */

public class StationTag extends BaseModel {
    private String countryCode;
    private String currencyCode;
    private String latitude;
    private String longitude;
    private String timeZoneCode;

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCurrencyCode() {
        return this.currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
    public String getTimeZoneCode() {
        return this.timeZoneCode;
    }

    public void setTimeZoneCode(String timeZoneCode) {
        this.timeZoneCode = timeZoneCode;
    }
}
