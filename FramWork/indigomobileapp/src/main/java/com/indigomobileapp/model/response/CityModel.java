package com.indigomobileapp.model.response;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 21-10-2016.
 */

public class CityModel extends BaseModel {
    private CityTag tag;
    private String value;

    public CityTag getTag() {
        return this.tag;
    }

    public void setTag(CityTag tag) {
        this.tag = tag;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
