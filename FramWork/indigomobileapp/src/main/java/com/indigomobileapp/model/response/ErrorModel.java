package com.indigomobileapp.model.response;


import kellton.framwork.model.BaseModel;

/**
 * Created by Ankur on 15/06/15.
 */
public class ErrorModel extends BaseModel {
    String message;
    String success;
    int statusCode;
    String error;
    String error_description;

    public String getMessage() {
        if (message == null) {
            if (error_description != null) {
                message = error_description;
            }
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }
}
