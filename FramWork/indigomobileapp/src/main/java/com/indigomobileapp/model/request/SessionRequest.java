package com.indigomobileapp.model.request;

/**
 * Created by ankur.goyal on 01-11-2016.
 */

public class SessionRequest extends BaseRequest{
    private String subscriptionKey;

    public String getSubscriptionKey() {
        return subscriptionKey;
    }

    public void setSubscriptionKey(String subscriptionKey) {
        this.subscriptionKey = subscriptionKey;
    }
}
