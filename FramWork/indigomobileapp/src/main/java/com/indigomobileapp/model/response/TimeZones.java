package com.indigomobileapp.model.response;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 24-10-2016.
 */

public class TimeZones extends BaseModel {
    private String key;
    private String localVariationMins;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLocalVariationMins() {
        return localVariationMins;
    }

    public void setLocalVariationMins(String localVariationMins) {
        this.localVariationMins = localVariationMins;
    }
}
