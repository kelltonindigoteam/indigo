package com.indigomobileapp.model.response;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 24-10-2016.
 */

public class CountryModel extends BaseModel {
    private String countryCode;
    private String countryName;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
