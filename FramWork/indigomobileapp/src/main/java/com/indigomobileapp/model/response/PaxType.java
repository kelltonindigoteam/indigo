package com.indigomobileapp.model.response;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 24-10-2016.
 */

public class PaxType extends BaseModel {
    private String key;
    private String value;
    private boolean allowed;
    private String createdAgentID;
    private String createdDate;
    private boolean inActive;
    private String maximumAge;
    private String minimumAge;
    private String modifiedAgentID;
    private String modifiedDate;
    private String name;
    private String nonAdult;
    private String state;
    private String typeCode;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    public String getCreatedAgentID() {
        return createdAgentID;
    }

    public void setCreatedAgentID(String createdAgentID) {
        this.createdAgentID = createdAgentID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isInActive() {
        return inActive;
    }

    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }

    public String getMaximumAge() {
        return maximumAge;
    }

    public void setMaximumAge(String maximumAge) {
        this.maximumAge = maximumAge;
    }

    public String getMinimumAge() {
        return minimumAge;
    }

    public void setMinimumAge(String minimumAge) {
        this.minimumAge = minimumAge;
    }

    public String getModifiedAgentID() {
        return modifiedAgentID;
    }

    public void setModifiedAgentID(String modifiedAgentID) {
        this.modifiedAgentID = modifiedAgentID;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNonAdult() {
        return nonAdult;
    }

    public void setNonAdult(String nonAdult) {
        this.nonAdult = nonAdult;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }
}
