package com.indigomobileapp.model.response;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 21-10-2016.
 */

public class StationModel extends BaseModel{
    private StationTag tag;
    private String value;

    public StationTag getTag() {
        return tag;
    }

    public void setTag(StationTag tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
