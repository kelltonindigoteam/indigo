package com.indigomobileapp.model.response.session;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 01-11-2016.
 */

public class SessionModel extends BaseModel{
    private String session_token;

    public String getSession_token() {
        return session_token;
    }

    public void setSession_token(String session_token) {
        this.session_token = session_token;
    }
}
