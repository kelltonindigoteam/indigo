package com.indigomobileapp.model.response;

import kellton.framwork.model.BaseModel;

/**
 * Created by ankur.goyal on 21-10-2016.
 */

public class CityTag extends BaseModel {
    private String cityCode;
    private String countryCode;
    private String name;
    private String provinceStateCode;
    private String shortName;

    public String getCityCode() {
        return this.cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvinceStateCode() {
        return this.provinceStateCode;
    }

    public void setProvinceStateCode(String provinceStateCode) {
        this.provinceStateCode = provinceStateCode;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
