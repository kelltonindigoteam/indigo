package com.indigomobileapp.model.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ankur.goyal on 01-02-2016.
 * Base class of all request
 */
public class BaseRequest implements Parcelable {
    public BaseRequest() {
        // nothing to do here
    }
    public BaseRequest(Parcel source) {

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
    public static Creator<BaseRequest> CREATOR = new Creator<BaseRequest>() {
        @Override
        public BaseRequest createFromParcel(Parcel source) {
            return new BaseRequest(source);
        }

        @Override
        public BaseRequest[] newArray(int size) {
            return new BaseRequest[size];
        }
    };

    /***
     * Get json with a param
     * @param param
     * @return
     */
    public String getPayLoadString(String param) {
        String value= new Gson().toJson(this);
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json = new JSONObject(value);
            jsonObject.put(param,json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  jsonObject.toString();
    }

    /****
     * Get jason
     * @return
     */
    public String getPayLoadString() {
        String value= new Gson().toJson(this);
        return  value;
    }
}
