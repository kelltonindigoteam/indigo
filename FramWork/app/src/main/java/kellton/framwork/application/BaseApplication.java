package kellton.framwork.application;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import kellton.framwork.database.DbHelper;

/**
 * Created by ankur.goyal on 10-08-2016.
 */

public abstract class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }

    /***
     * Data base creation
     *
     * @return
     */
    public DbHelper getDbHelper() {
        String dbName = null;
        int dbVersion = -1;
        try {
            ApplicationInfo ai = getApplicationMetaData();
            Bundle bundle = ai.metaData;
            dbName = bundle.getString("DB_NAME");
            dbVersion = bundle.getInt("DB_VERSION");
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NullPointerException e) {
        }
        if (dbName == null || dbVersion <= 0) {
            throw new IllegalStateException("DB name or version is not configured. Please check <meta-data> in manifest file");
        }
        return DbHelper.getDbHelper(this, dbName, null, dbVersion);
    }
    /***
     * Password Protected Data base creation
     * @return
     */
   /* public CipherDbHelper getCipherDbHelper() {
        String dbName = null;
        int dbVersion = -1;
        try {
            ApplicationInfo ai = getApplicationMetaData();
            Bundle bundle = ai.metaData;
            dbName = bundle.getString("DB_NAME");
            dbVersion = bundle.getInt("DB_VERSION");
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NullPointerException e) {
        }
        if (dbName == null || dbVersion <= 0) {
            throw new IllegalStateException("DB name or version is not configured. Please check <meta-data> in manifest file");
        }
        return CipherDbHelper.getCipherDbHelper(this, dbName, null, dbVersion);
    }*/

    /***
     * Read application meta data
     *
     * @return
     * @throws PackageManager.NameNotFoundException
     */
    public ApplicationInfo getApplicationMetaData()
            throws PackageManager.NameNotFoundException {
        ApplicationInfo ai = getPackageManager().getApplicationInfo(this.getPackageName(), PackageManager.GET_META_DATA);
        return ai;
    }

    protected abstract void initialize();
}
