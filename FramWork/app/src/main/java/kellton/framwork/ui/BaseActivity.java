package kellton.framwork.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import kellton.framwork.R;
import kellton.framwork.font.FontContextWrapper;
import kellton.framwork.interfaces.IFragmentBackButtonListener;
import kellton.framwork.interfaces.IScreen;
import kellton.framwork.util.StringUtils;

/**
 * Created by ankur.goyal on 10-08-2016.
 * Base Activity for all activity
 */

public abstract class BaseActivity extends AppCompatActivity implements IScreen {
    /**
     * Base fragment instance
     *
     * @link kellton.framwork.ui.BaseFragment
     */
    private BaseFragment mBaseFragment;
    /***
     * Fragment back stack management at hardware back button
     *
     * @link kellton.framwork.interfaces.IFragmentBackButtonListener
     */
    private IFragmentBackButtonListener iFragmentBackButtonListener;

    public void setBaseFragment(Fragment pFragment) {
        if (pFragment instanceof BaseFragment) {
            mBaseFragment = (BaseFragment) pFragment;
            setFragmentBackButtonListener((IFragmentBackButtonListener) mBaseFragment);
        }
    }

    /***
     * Font change when an activity created.
     *
     * @param newBase
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(FontContextWrapper.wrap(newBase));
    }

    /***
     * set fragment hardware back button listener
     *
     * @param iFragmentBackButtonListener
     */
    private void setFragmentBackButtonListener(IFragmentBackButtonListener iFragmentBackButtonListener) {
        this.iFragmentBackButtonListener = iFragmentBackButtonListener;
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        /**Check for click on any fragment,if fragment onBackPressed return true than, we take our custom action on device back button.*/
        if (mBaseFragment != null && mBaseFragment.onBackPressed()) {
            return;
            /**Check if only one fragment in stack.*/
        } else if (manager != null && manager.getBackStackEntryCount() == 1) {
            finish();
            /**If there is many fragment in stack than remove the top most from stack.*/
        } else if (manager != null && manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
        } else {
            /**Other wise call the parent activity back press method*/
            super.onBackPressed();
        }
    }

    /**
     * Get current fragment in containerId
     *
     * @param containerId = R.id.container
     */
    public Fragment getFragment(int containerId) {
        return getSupportFragmentManager().findFragmentById(containerId);
    }

    /**
     * Add a fragment
     *
     * @param fragment        Fragment which we want to add
     * @param bundle          Any bundle data
     * @param addToBackStack  Is fragment add in back stack
     * @param anim            Is there any animation
     * @param retainInstance  Is we want to set retain instance
     * @param currentFragment Current visible fragment
     * @param containerId     container layout id
     * @param tag             fragment tag
     */
    public void addFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment currentFragment, int containerId, String tag) {
        // check bundle data
        if (bundle != null){
            fragment.setArguments(bundle);
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        //check for tag, if tag is null or empty than use class name as tag
        if (StringUtils.isNullOrEmpty(tag)) {
            tag = fragment.getClass().getSimpleName();
        }
        // find fragment in back stack
        Fragment already = fragmentManager.findFragmentByTag(tag);
        fragmentManager.popBackStackImmediate(tag, 0);
        // if fragment not exist in back stack
        if (already == null) {
            if (anim) {
                ft.setCustomAnimations(R.anim.slide_from_right, 0, 0, R.anim.slide_to_right);
            }
            // add fragment in container
            ft.add(containerId, fragment, tag);
            fragment.setRetainInstance(retainInstance);
            // if addToBackStack true than add fragment in back stack
            if (addToBackStack)
                ft.addToBackStack(tag);
            // hide the current fragment
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            try {
                // commit this transaction
                ft.commit();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        // if fragment already exist than nothing to do
        else {
            fragment = already;
        }
        // set listener on fragment
        setBaseFragment(fragment);
    }

    /**
     * replace a fragment
     *
     * @param fragment        Fragment which we want to add
     * @param bundle          Any bundle data
     * @param addToBackStack  Is fragment add in back stack
     * @param anim            Is there any animation
     * @param retainInstance  Is we want to set retain instance
     * @param currentFragment Current visible fragment
     * @param containerId     container layout id
     * @param tag             fragment tag
     */
    public void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment currentFragment, int containerId, String tag) {
        // check bundle data
        if (bundle != null){
            fragment.setArguments(bundle);
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        //check for tag, if tag is null or empty than use class name as tag
        if (StringUtils.isNullOrEmpty(tag)) {
            tag = fragment.getClass().getSimpleName();
        }
        // find fragment in back stack
        Fragment already = fragmentManager.findFragmentByTag(tag);
        fragmentManager.popBackStackImmediate(tag, 0);
        // if fragment not exist in back stack
        if (already == null) {
            if (anim) {
                ft.setCustomAnimations(R.anim.slide_from_right, 0, 0, R.anim.slide_to_right);
            }
            // add fragment in container
            ft.replace(containerId, fragment, tag);
            fragment.setRetainInstance(retainInstance);
            // if addToBackStack true than add fragment in back stack
            if (addToBackStack)
                ft.addToBackStack(tag);
            // hide the current fragment
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            try {
                // commit this transaction
                ft.commit();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        // if fragment already exist than nothing to do
        else {
            fragment = already;
        }
        // set listener on fragment
        setBaseFragment(fragment);
    }
}
