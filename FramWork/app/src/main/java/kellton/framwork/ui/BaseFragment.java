package kellton.framwork.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import kellton.framwork.interfaces.IFragmentBackButtonListener;

/**
 * Created by ankur.goyal on 11-08-2016.
 * BaseFragment for all fragment
 */

public abstract class BaseFragment extends Fragment implements IFragmentBackButtonListener {
    @Override
    public boolean onBackPressed() {
        return false;
    }

    /***
     * Remove a fragment from back stack
     */
    public void removeFragment() {
        if (getActivity() == null) {
            return;
        }
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(this);
        trans.commit();
        manager.popBackStack();
        manager.executePendingTransactions();
    }

}
