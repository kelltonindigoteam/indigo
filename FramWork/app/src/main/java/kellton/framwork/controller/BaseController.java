package kellton.framwork.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.util.HashMap;
import java.util.List;

import kellton.framwork.interfaces.IScreen;
import kellton.framwork.volley.HttpsTrustManager;

/**
 * Created by ankur.goyal on 19-09-2016.
 * Base controller
 */

public abstract class BaseController extends Fragment {
    //set data on ui thread
    private IScreen iScreen;
    //Request event action constant
    public static final String EVENT_ACTION = "Action";
    //Request url constant
    public static final String EVENT_URL = "url";
    // request body constant
    public static final String REQUEST = "request";
    // header key constant
    public static final String HEADER_VALUE = "header_value";
    // header value constant
    public static final String HEADER_KEY = "header_key";


    // Request event action value
    protected int eventAction = 0;
    // header map
    protected HashMap<String, String> header = new HashMap<>();
    // request body
    protected String request = null;
    // request url
    protected String url = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.iScreen = (IScreen) context;
        iScreen.showProgress();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iScreen = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //SSL
        HttpsTrustManager.allowAllSSL();
        setRetainInstance(true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            eventAction = bundle.getInt(EVENT_ACTION);
            url = bundle.getString(EVENT_URL);
            request = bundle.getString(REQUEST);
            List<String> headerKey = bundle.getStringArrayList(HEADER_KEY);
            List<String> headerValue = bundle.getStringArrayList(HEADER_VALUE);
            if (headerKey != null) {
                for (int i = 0; i < headerKey.size(); i++) {
                    header.put(headerKey.get(i), headerValue.get(i));
                }
            }
        }
    }

    /***
     * return IScreen instance
     * @return
     */
    public IScreen getIScreen() {
        return iScreen;
    }


}
