package kellton.framwork.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ankur.goyal on 11-08-2016.
 */

public class BaseModel implements Parcelable {
    private int primaryKey;

    /**
     * no-arg constructor
     */
    public BaseModel() {
        // nothing to do here
    }

    /**
     * @return the primaryKey
     */
    public int getPrimaryKey() {
        return primaryKey;
    }

    /**
     * @param primaryKey the primaryKey to set
     */
    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }
    /**
     * Reconstruct from the Parcel
     *
     * @param in
     */
    protected BaseModel(Parcel in) {
        primaryKey = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(primaryKey);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaseModel> CREATOR = new Creator<BaseModel>() {
        @Override
        public BaseModel createFromParcel(Parcel in) {
            return new BaseModel(in);
        }

        @Override
        public BaseModel[] newArray(int size) {
            return new BaseModel[size];
        }
    };
}
