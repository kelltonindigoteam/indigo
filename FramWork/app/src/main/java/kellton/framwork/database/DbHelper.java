package kellton.framwork.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by ankur.goyal on 11-08-2016.
 */

public class DbHelper extends SQLiteOpenHelper {
    /**
     * DbHelper instance
     */
    private static DbHelper dbHelper = null;
    /***
     * Database instance
     */
    private SQLiteDatabase mWritableDatabase;
    /**
     * Array of Table create queries...
     */
    public static final ArrayList<String> DB_SQL_CREATE_TABLE_QUERIES = new ArrayList<String>();
    /**
     * Array of Table drop queries...
     */
    public static final ArrayList<String> DB_SQL_DROP_TABLE_QUERIES = new ArrayList<String>();
    /**
     * Array of table upgrade query...
     */
    public static final ArrayList<String> DB_SQL_UPGARDE_QUERIES = new ArrayList<String>();
    public static DbHelper getDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        if (dbHelper == null) {
            synchronized (DbHelper.class) {
                if (dbHelper == null) {
                    dbHelper = new DbHelper(context, name, factory, version);
                }
            }
        }
        return dbHelper;
    }

    private DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // create table
        for (String table : DB_SQL_CREATE_TABLE_QUERIES) {
            sqLiteDatabase.execSQL(table);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop tables if any
        for (String table : DB_SQL_DROP_TABLE_QUERIES) {
            sqLiteDatabase.execSQL(table);
        }
       // Upgrade table if any
        for (String table : DB_SQL_UPGARDE_QUERIES) {
            sqLiteDatabase.execSQL(table);
        }

        onCreate(sqLiteDatabase);
    }

    /**
     * DataBase writable instance
     * @return
     */
    public SQLiteDatabase getWriteInstance() {
        return this.getWritableDatabase();
    }
}
