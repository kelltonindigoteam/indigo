package kellton.framwork.interfaces;

/**
 * Created by ankur.goyal on 10-08-2016.
 * Fragment back stack management on Hardware back button.
 */

public interface IFragmentBackButtonListener {
    boolean onBackPressed();
}
