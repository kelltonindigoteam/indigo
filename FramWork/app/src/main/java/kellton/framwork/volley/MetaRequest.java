package kellton.framwork.volley;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ankur.goyal on 01-11-2016.
 */

public abstract class MetaRequest<T> extends JsonRequest<T> {
    private final Class<T> mClass;
    private final Gson mGson;
    private Map<String, String> metaMap = new HashMap<>();

    public MetaRequest(String url, Map<String, String> mRequestHeaders, String jsonPayload, Class<T> mClass, Map<String, String> metaMap, Response.ErrorListener errorListener) {
        super(url, mRequestHeaders, jsonPayload, errorListener);
        this.mClass = mClass;
        this.metaMap = metaMap;
        mGson = new Gson();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            JSONObject jsonResponse = new JSONObject();
            for (Map.Entry<String, String> entry : metaMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                String session = response.headers.get(key);
                jsonResponse.put(value, session);
            }
            return Response.success(mGson.fromJson(jsonResponse.toString(), mClass), HttpHeaderParser.parseCacheHeaders(response));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

}
