package kellton.framwork.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/***
 * @param <T>
 * @author ankur.goyal
 *         Custom request to make multipart header and upload file.
 */

public abstract class MultipartRequest<T> extends Request<T> {
    private static final String TAG = MultipartRequest.class.getSimpleName();
    protected NetworkResponse mResponse;
    private HttpEntity mHttpEntity;
    private Priority mPriority;
    private final Gson mGson;
    private final Class mClass;
    private String boundary = null;
    private Map<String, String> mHeaders;
    private String filePartName;
    /**
     * Default constructor with predefined header and post method.
     *
     * @param url           request destination
     * @param filePath      Image file path
     * @param filePartName  key name
     * @param headers       predefined custom header
     * @param body          body param
     * @param errorListener on error http or library timeout
     */
    public MultipartRequest(String url, String filePath, String filePartName, Map<String, String> headers, Map<String, String> body, Class mClass, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        File file = new File(filePath);
        this.filePartName = filePartName;
        mHttpEntity = buildMultipartEntity(file, body);
        this.mHeaders = headers;
        mGson = new Gson();
        this.mClass = mClass;
    }

    /***
     * Constructor with option method and file name configuration.
     *
     * @param method
     * @param url
     * @param file
     * @param headers
     * @param body
     * @param mClass
     * @param errorListener
     */
    public MultipartRequest(int method, String url, File file, String filePartName, Map<String, String> headers, Map<String, String> body, Class mClass, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.filePartName = filePartName;
        mHttpEntity = buildMultipartEntity(file, body);
        this.mHeaders = headers;
        mGson = new Gson();
        this.mClass = mClass;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return (mHeaders != null) ? mHeaders : super.getHeaders();
    }

    /***
     * Parse string map into MultipartEntityBuilder
     * @param file
     * @param stringPartMap
     * @return
     */
    private HttpEntity buildMultipartEntity(File file, Map<String, String> stringPartMap) {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        boundary = "===" + System.currentTimeMillis() + "===";
        if (stringPartMap != null) {
            if (file != null)
                builder.addBinaryBody(filePartName, file);
        } else {
            builder.addBinaryBody(filePartName, file, ContentType.create("multipart/form-data; boundary=" + boundary), file.getName());
        }
        if (stringPartMap != null && stringPartMap.size() > 0) {
            for (Map.Entry entry : stringPartMap.entrySet()) {
                builder.addTextBody(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
            }
        }
        return builder.build();
    }

    public void setPriority(Priority mPriority) {
        this.mPriority = mPriority;
    }

    @Override
    public Priority getPriority() {
        return this.mPriority;
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            this.mResponse = response;
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.d(TAG, "Response: " + json.toString());
            return (Response<T>) Response.success(mGson.fromJson(json, mClass), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

}
