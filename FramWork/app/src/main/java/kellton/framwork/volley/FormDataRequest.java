package kellton.framwork.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by ankur.goyal on 24-08-2016.
 * Form data request
 */

public abstract class FormDataRequest<T> extends Request<T> {
    private Map<String, String> mHeaderMap;
    private Map<String, String> mBodyMap;
    private final Gson mGson;
    private final Class<T> mClazz;
    protected NetworkResponse mResponse;

    /***
     * @param method
     * @param url
     * @param mHeaderMap
     * @param mBodyMap
     * @param clazz
     * @param listener
     */

    public FormDataRequest(int method, String url, Map<String, String> mHeaderMap, Map<String, String> mBodyMap, Class<T> clazz, Response.ErrorListener listener) {
        super(method, url, listener);
        this.mBodyMap = mBodyMap;
        this.mHeaderMap = mHeaderMap;
        this.mClazz = clazz;
        mGson = new Gson();
    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=UTF-8";
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return mBodyMap;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (mHeaderMap == null)
            return super.getHeaders();
        return mHeaderMap;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            this.mResponse = response;
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.e("JsonResponse", json);
            return Response.success(mGson.fromJson(json, mClazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}
