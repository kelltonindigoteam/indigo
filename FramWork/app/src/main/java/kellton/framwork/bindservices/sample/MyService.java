package kellton.framwork.bindservices.sample;

import android.os.Message;
import android.util.Log;
import java.util.Timer;
import java.util.TimerTask;

import kellton.framwork.bindservices.AbstractService;

/**
 * Created by ankur.goyal on 19-08-2016.
 */

public class MyService extends AbstractService {
    public static final int MSG_INCREMENT = 1;
    public static final int MSG_COUNTER = 2;
    private Timer timer = new Timer();
    private int counter = 0, incrementBy = 1;

    @Override
    public void onStartService() {
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                onTimerTick();
            }
        }, 0, 250L);
    }

    @Override
    public void onStopService() {
        if (timer != null) {
            timer.cancel();
        }
        counter = 0;
    }

    @Override
    public void onReceiveMessage(Message msg) {
        if (msg.what == MSG_INCREMENT) {
            incrementBy = msg.arg1;
        }
    }

    private void onTimerTick() {
        try {
            counter += incrementBy;
            // Send data as simple integer
            Message message = Message.obtain(null, MSG_COUNTER, counter, 0);
            send(message);
        } catch (Throwable t) { //you should always ultimately catch all exceptions in timer tasks.
            Log.e("TimerTick", "Timer Tick Failed.", t);
        }
    }
}
