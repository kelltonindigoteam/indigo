package kellton.framwork.bindservices;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;

/**
 * 1. Implement a service by inheriting from AbstractService
 * 2. Add a ServiceManager to your activity
 * - Control the service with ServiceManager.start() and .stop()
 * - Send messages to the service via ServiceManager.send()
 * - Receive messages with by passing a Handler in the constructor
 * 3. Send and receive messages on the service-side using send() and onReceiveMessage()
 * Created by ankur.goyal on 19-08-2016.
 */

public abstract class AbstractService extends Service {
    static final int MSG_REGISTER_CLIENT = 9991;
    static final int MSG_UNREGISTER_CLIENT = 9992;
    // Keeps track of all current registered clients.
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    // Target we publish for clients to send messages to IncomingHandler.
    final Messenger mMessenger = new Messenger(new InComingHandler());

    public abstract void onStartService();

    public abstract void onStopService();

    public abstract void onReceiveMessage(Message msg);

    private class InComingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                default:
                    onReceiveMessage(msg);
            }

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        onStartService();
        Log.i("AbstractService", "Service Started.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("AbstractService", "Received start id " + startId + ": " + intent);
        return START_STICKY; // run until explicitly stopped.
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        onStopService();

        Log.i("AbstractService", "Service Stopped.");
    }

    public void send(Message msg) {
        for (int i = mClients.size() - 1; i >= 0; i--) {
            try {
                Log.i("AbstractService", "Sending message to clients: " + msg);
                mClients.get(i).send(msg);
            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                Log.e("AbstractService", "Client is dead. Removing from list: " + i);
                mClients.remove(i);
            }
        }
    }

}
