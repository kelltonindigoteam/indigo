package kellton.framwork.bindservices.sample;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.view.View;


import kellton.framwork.bindservices.ServiceManager;
import kellton.framwork.ui.BaseActivity;

/**
 * Created by ankur.goyal on 19-08-2016.
 */

public class MyActivity extends BaseActivity implements View.OnClickListener {
    private ServiceManager mServiceManager;
    Handler handler = new MyHandler();
    int incrementBy = 10;

    @Override
    public void updateUi(boolean status, int actionID, Object serviceResponse) {

    }

    @Override
    public void getData(int actionID) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceManager = new ServiceManager(this, MyService.class, handler);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceManager.start();
    }

    @Override
    public void onClick(View view) {
        Message message = Message.obtain(null, MyService.MSG_INCREMENT, incrementBy);
        try {
            mServiceManager.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MyService.MSG_COUNTER:
                    int counter = msg.arg1;
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceManager.stop();
    }
}
