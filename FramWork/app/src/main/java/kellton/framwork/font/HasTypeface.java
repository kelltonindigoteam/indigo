package kellton.framwork.font;

import android.graphics.Typeface;

public interface HasTypeface {
    void setTypeface(Typeface typeface);
}
