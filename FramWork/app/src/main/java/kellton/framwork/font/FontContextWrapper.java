package kellton.framwork.font;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

/**
 * @author ankur.goyal
 */
public class FontContextWrapper extends ContextWrapper {

    private FontLayoutInflater mInflater;

    private final int mAttributeId;

    /**
     * Uses the default configuration from {@link kellton.framwork.font.FontConfig}
     *
     * Remember if you are defining default in the
     * {@link kellton.framwork.font.FontConfig} make sure this is initialised before
     * the activity is created.
     *
     * @param base ContextBase to Wrap.
     * @return ContextWrapper to pass back to the activity.
     */
    public static ContextWrapper wrap(Context base) {
        return new FontContextWrapper(base);
    }

    /**
     *
     * @param activity The activity the original that the ContextWrapper was attached too.
     * @param parent   Parent view from onCreateView
     * @param view     The View Created inside onCreateView or from super.onCreateView
     * @param name     The View name from onCreateView
     * @param context  The context from onCreateView
     * @param attr     The AttributeSet from onCreateView
     * @return The same view passed in, or null if null passed in.
     */
    public static View onActivityCreateView(Activity activity, View parent, View view, String name, Context context, AttributeSet attr) {
        return get(activity).onActivityCreateView(parent, view, name, context, attr);
    }

    /**
     * Get the Calligraphy Activity Fragment Instance to allow callbacks for when views are created.
     *
     * @param activity The activity the original that the ContextWrapper was attached too.
     * @return Interface allowing you to call onActivityViewCreated
     */
    static FontActivityFactory get(Activity activity) {
        if (!(activity.getLayoutInflater() instanceof FontLayoutInflater)) {
            throw new RuntimeException("This activity does not wrap the Base Context! See FontContextWrapper.wrap(Context)");
        }
        return (FontActivityFactory) activity.getLayoutInflater();
    }

    /**
     * Uses the default configuration from {@link kellton.framwork.font.FontConfig}
     *
     * Remember if you are defining default in the
     * {@link kellton.framwork.font.FontConfig} make sure this is initialised before
     * the activity is created.
     *
     * @param base ContextBase to Wrap
     */
    FontContextWrapper(Context base) {
        super(base);
        mAttributeId = FontConfig.get().getAttrId();
    }

    @Override
    public Object getSystemService(String name) {
        if (LAYOUT_INFLATER_SERVICE.equals(name)) {
            if (mInflater == null) {
                mInflater = new FontLayoutInflater(LayoutInflater.from(getBaseContext()), this, mAttributeId, false);
            }
            return mInflater;
        }
        return super.getSystemService(name);
    }

}
