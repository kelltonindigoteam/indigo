package kellton.framwork.font;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author ankur.goyal
 */
interface FontActivityFactory {
    View onActivityCreateView(View parent, View view, String name, Context context, AttributeSet attrs);
}
