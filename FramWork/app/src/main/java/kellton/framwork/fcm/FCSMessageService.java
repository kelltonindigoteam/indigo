package kellton.framwork.fcm;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;


/**
 * Created by ankur.goyal on 12-08-2016.
 */

public abstract class FCSMessageService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //check if there is notification key in response
        if(remoteMessage.getNotification()!=null) {

        }
        sendNotification(remoteMessage.getData());
    }

    public abstract void sendNotification(Map<String,String> data);
}
