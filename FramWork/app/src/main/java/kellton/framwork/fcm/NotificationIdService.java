package kellton.framwork.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by ankur.goyal on 12-08-2016.
 */

public abstract class NotificationIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("ID", "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);

    }
    public abstract void sendRegistrationToServer(String refreshedToken);
}
