# **FrameWork for Android**

### **Requirements**
- Android 4.1 or higher
- compileSdkVersion 24 or higher
- buildToolsVersion "24.0.1" or higher
- Gradle 2.2.0 or higher.


##**Gradle dependency**
```
repositories {
    mavenCentral()
}
dependencies {
//Volley Network Lib
compile 'com.mcxiaoke.volley:library:1.0.16'

//Google GSON
compile 'com.google.code.gson:gson:2.2.4'

//Google Play service
compile 'com.google.android.gms:play-services-gcm:7.3.0'

//Support v7
compile 'com.android.support:appcompat-v7:23.1.1'

//FCM
compile 'com.google.firebase:firebase-messaging:9.4.0'

//Run Time Permission
compile 'com.github.hotchemi:permissionsdispatcher:2.1.3'
apt 'com.github.hotchemi:permissionsdispatcher-processor:2.1.3'

// Cipher data base
compile 'net.zetetic:android-database-sqlcipher:3.5.3@aar'

//For Multipart request
compile "org.apache.httpcomponents:httpcore:4.2.4"
compile "org.apache.httpcomponents:httpmime:4.3"
   
}
```
##**Font** 
```
Add this code in your application class

FontConfig.initDefault(new FontConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand_Regular.otf")
                .setFontAttrId(R.attr.fontPath).build()
        );
 Where font defind in your assest folder under fonts directory
 ```       
**Custom font per TextView**
```
   <TextView
       android:text="@string/hello_world"
       android:layout_width="wrap_content"
       android:layout_height="wrap_content"
       fontPath="fonts/Roboto-Bold.ttf"/>
```  
 **Custom font in Styles**
```
 <style name="TextViewCustomFont">
     <item name="fontPath">fonts/RobotoCondensed-Regular.ttf</item>
 </style>
 ```
  **Custom font defined in Theme**
 ```
 <style name="AppTheme" parent="android:Theme.Holo.Light.DarkActionBar">
     <item name="android:textViewStyle">@style/AppTheme.Widget.TextView</item>
 </style>
 
 <style name="AppTheme.Widget"/>
 
 <style name="AppTheme.Widget.TextView" parent="android:Widget.Holo.Light.TextView">
     <item name="fontPath">fonts/Roboto-ThinItalic.ttf</item>
 </style>
```
##**DataBase**

**Define meta data in android manifest file**
```
<meta-data
      android:name="DB_NAME"
      android:value="name of db" />
<meta-data
      android:name="DB_VERSION"
      android:value="version of db" />// must be integer
      
 Extend BaseApplication class to your applcation class
      
 call getDbHelper() method
 DbHelper helper = new DbHelper();
 
 DbHelper object provide you DB instance
  
```
**Table Creation**
```
1.Every Table should extend BaseTable.java.
2.Add your table creation queary in DB_SQL_CREATE_TABLE_QUERIES arraylist.
3.For insert and update model should extend BaseModel.java.
```

**Table upgrade** 
```
change Database version int in manifest
Add your upgrade query in DB_SQL_UPGARDE_QUERIES arraylist
```
**Table Delete(Drop)**
```
chnage DataBase version int in menifest 
Add your Delete query in DB_SQL_DROP_TABLE_QUERIES arraylist
```
**Cipher Database**
```
User Cipher DBHandler in your application class.

```
**Note**
```
Table creation, upgrade and delete queries addition in thier arraylist always before DbHelper object creation
Show always call as a first line in your Application class initialize() method.
```
##**FCM**
**Getting a configuration key**
```
1.Go to Firebase console and create a new project.
https://firebase.google.com/console/
2.Put your app name and select your country.
3.Enter your application package name(Apllication Id) and SHA1 of signing key.
4.After clicking add app you will get google-services.json file.
```
**Project settings**
```
1.Now come back to your android project. Go to app folder and paste google-services.json file.
2.extend NotificationIdService.java and override sendRegistrationToServer method.
3.Now write a code to send your token from application to server.
4.extend FCSMessageService.java to read message.
```
### 1. Run Time Permission

PermissionsDispatcher introduces only a few annotations, keeping its general API concise:

> NOTE: Annotated methods must not be `private`.

|Annotation|Required|Description|
|---|---|---|
|`@RuntimePermissions`|**✓**|Register an `Activity` or `Fragment` to handle permissions|
|`@NeedsPermission`|**✓**|Annotate a method which performs the action that requires one or more permissions|
|`@OnShowRationale`||Annotate a method which explains why the permission/s is/are needed. It passes in a `PermissionRequest` object which can be used to continue or abort the current permission request upon user input|
|`@OnPermissionDenied`||Annotate a method which is invoked if the user doesn't grant the permissions|
|`@OnNeverAskAgain`||Annotate a method which is invoked if the user chose to have the device "never ask again" about a permission|

```java
@RuntimePermissions
public class MainActivity extends AppCompatActivity {

    @NeedsPermission(Manifest.permission.CAMERA)
    void showCamera() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.sample_content_fragment, CameraPreviewFragment.newInstance())
                .addToBackStack("camera")
                .commitAllowingStateLoss();
    }

    @OnShowRationale(Manifest.permission.CAMERA)
    void showRationaleForCamera(PermissionRequest request) {
        new AlertDialog.Builder(this)
            .setMessage(R.string.permission_camera_rationale)
            .setPositiveButton(R.string.button_allow, (dialog, button) -> request.proceed())
            .setNegativeButton(R.string.button_deny, (dialog, button) -> request.cancel())
            .show();
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    void showDeniedForCamera() {
        Toast.makeText(this, R.string.permission_camera_denied, Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    void showNeverAskForCamera() {
        Toast.makeText(this, R.string.permission_camera_neverask, Toast.LENGTH_SHORT).show();
    }
}
```

### 2. Delegate to generated class

Upon compilation, PermissionsDispatcher generates a class for `MainActivityPermissionsDispatcher`, which you can use to safely access these permission-protected methods.

The only step you have to do is delegating the work to this helper class:

```java
@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    findViewById(R.id.button_camera).setOnClickListener(v -> {
      // NOTE: delegate the permission handling to generated method
      MainActivityPermissionsDispatcher.showCameraWithCheck(this);
    });
    findViewById(R.id.button_contacts).setOnClickListener(v -> {
      // NOTE: delegate the permission handling to generated method
      MainActivityPermissionsDispatcher.showContactsWithCheck(this);
    });
}

@Override
public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    // NOTE: delegate the permission handling to generated method
    MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
}
```